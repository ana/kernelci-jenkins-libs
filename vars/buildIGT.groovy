def call(arch) {

  switch( "${arch}" ) {
    case "armel":
        debian_arch = "armhf"
        toolchain_arch = "arm-linux-gnueabihf"
        break
    case "x86":
        debian_arch = "i386"
        toolchain_arch = "i386-linux-gnu"
        break
    case "x86_64":
        debian_arch = "amd64"
        toolchain_arch = "x86_64-linux-gnu"
        break
    case "arm64":
        debian_arch = "${arch}"
        toolchain_arch = "aarch64-linux-gnu"
        break
    default:
        break
  }

  sh "mkdir -p ${PIPELINE_VERSION}/${arch}/libdrm"
  sh """
    cd ${PIPELINE_VERSION}/${arch}/libdrm/
    git git://anongit.freedesktop.org/mesa/drm .
    mkdir -p usr # Build to usr so it's in the right place when we add it to cpio
    PKG_CONFIG_PATH=/usr/lib/${toolchain_arch}/pkgconfig/ sh autogen.sh --host=${toolchain_arch} --enable-intel --prefix=\$(pwd)/usr/
    make -j\$(nproc) V=1
    make -j\$(nproc) install V=1
    ls usr/lib/lib*.so* | cpio -H newc -v -o | gzip -c - > libdrm.cpio.gz
    cd ..
  """
  stash includes: "${PIPELINE_VERSION}/${arch}/libdrm/libdrm.cpio.gz", name: "libdrm-${arch}"
  archiveArtifacts artifacts: "${PIPELINE_VERSION}/${arch}/libdrm/libdrm.cpio.gz", fingerprint: true
  
  sh "mkdir -p ${PIPELINE_VERSION}/${arch}/igt-gpu-tools"
  sh """
    git clone git://anongit.freedesktop.org/drm/igt-gpu-tools .
    PKG_CONFIG_PATH=/usr/lib/${toolchain_arch}/pkgconfig/:\$(pwd)/../libdrm/usr/lib/pkgconfig sh autogen.sh --host=${toolchain_arch}
    make -j\$(nproc) V=1
    mkdir -p usr/bin
    cp tests/core_auth tests/core_get_client_auth tests/core_getclient tests/core_getstats tests/core_getversion tests/core_prop_blob tests/core_setmaster_vs_auth tests/drm_read tests/kms_addfb_basic tests/kms_atomic tests/kms_flip_event_leak tests/kms_setmode tests/kms_vblank tests/kms_frontbuffer_tracking tests/kms_flip usr/bin/.
    ${toolchain_arch}-strip usr/bin/*
    find -H usr | cpio -H newc -v -o | gzip -c - > igt.cpio.gz
  """
  stash includes: "${PIPELINE_VERSION}/${arch}/igt-gpu-tools/igt.cpio.gz", name: "igt-${arch}"
  archiveArtifacts artifacts: "${PIPELINE_VERSION}/${arch}/igt-gpu-tools/igt.cpio.gz", fingerprint: true
}

