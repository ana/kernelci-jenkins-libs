def call(arch) {

  unstash "rootfs-${arch}"
  unstash "libdrm-${arch}"
  unstash "igt-${arch}"
  sh """
    cd ${PIPELINE_VERSION}/${arch}
    gunzip rootfs.cpio.gz
    mkdir ramdisk && cd ramdisk
    zcat ../librm/libdrm.cpio.gz | cpio -iud
    zcat ../igt-gpu-tools/igt.cpio.gz | cpio -iud
    find . | cpio --format=newc --verbose --create --append --file=../rootfs.cpio
    cd ..
    gzip -c rootfs.cpio > rootfs-igt-${arch}.cpio.gz
  """
  archiveArtifacts artifacts: "${PIPELINE_VERSION}/${arch}/rootfs-igt-${arch}.cpio.gz", fingerprint: true
}
