def call(arch) {

  switch( "${arch}" ) {
    case "armel":
        debian_arch = "armhf"
        toolchain_arch = "arm-linux-gnueabihf"
        break
    case "x86":
        debian_arch = "i386"
        toolchain_arch = "i386-linux-gnu"
        break
    case "x86_64":
        debian_arch = "amd64"
        toolchain_arch = "x86_64-linux-gnu"
        break
    case "arm64":
        debian_arch = "${arch}"
        toolchain_arch = "aarch64-linux-gnu"
        break
    default:
        break
  }

  sh "mkdir -p ${PIPELINE_VERSION}/${arch}/v4l2-compliance/"
  sh """
    cd ${PIPELINE_VERSION}/${arch}/v4l2-compliance/
    git clone git://linuxtv.org/v4l-utils.git .
    sh bootstrap.sh
    mkdir install
    PKG_CONFIG_PATH=/usr/lib/${toolchain_arch}/pkgconfig/ ./configure --host=${toolchain_arch} --prefix=\$(pwd)/install --with-udevdir=\$(pwd)/install/lib/udev
    make -j\$(nproc) V=1
    make V=1 install
    find install
    mkdir -p usr/bin usr/lib usr/lib/libv4l
    cp install/bin/* install/sbin/* usr/bin/.
    cp -rf install/lib/*.so* usr/lib/.
    cp -rf install/lib/libv4l/*.so* usr/lib/libv4l/.
    ${toolchain_arch}-strip usr/bin/* usr/lib/*.so* usr/lib/libv4l/*.so*
    find -H usr | cpio -H newc -v -o | gzip -c - > v4l2-compliance.cpio.gz
  """
  stash includes: " ${PIPELINE_VERSION}/${arch}/v4l2-compliance/v4l2-compliance.cpio.gz", name: "v4l2-compliance-${arch}"
  archiveArtifacts artifacts: " ${PIPELINE_VERSION}/${arch}/v4l2-compliance/v4l2-compliance.cpio.gz", fingerprint: true
}

