def call(arch, yaml_file, extra_packages="") {
  sh "mkdir -p ${PIPELINE_VERSION}/${arch}"

  switch( "${arch}" ) {
    case "armel":
        debian_arch = "armhf"
        break
    case "x86":
        debian_arch = "i386"
        break
    case "x86_64":
        debian_arch = "amd64"
        break
    default:
        debian_arch = "${arch}"
        break
  }
  sh "debos -t architecture:${debian_arch} -t basename:${PIPELINE_VERSION}/${arch}/ -t extra_packages:${extra_packages} ${yaml_file}"
  stash includes: "${PIPELINE_VERSION}/${arch}/rootfs.cpio.gz", name: "rootfs-${arch}"
  archiveArtifacts artifacts: "${PIPELINE_VERSION}/${arch}/rootfs.cpio.gz", fingerprint: true
}
