def call(arch) {

  unstash "rootfs-${arch}"
  unstash "v4l2-compliance-${arch}"
  sh """
    cd ${PIPELINE_VERSION}/${arch}
    gunzip rootfs.cpio.gz
    mkdir ramdisk && cd ramdisk
    zcat ../v4l2-compliance/v4l2-compliance.cpio.gz | cpio -iud
    find . | cpio --format=newc --verbose --create --append --file=../rootfs.cpio
    cd ..
    gzip -c rootfs.cpio > rootfs-v4l2-${arch}.cpio.gz
  """
  archiveArtifacts artifacts: "${PIPELINE_VERSION}/${arch}/rootfs-v4l2-${arch}.cpio.gz", fingerprint: true
}

